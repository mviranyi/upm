﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPM.Model
{
    class ConfigModel
    {
        public List<RepositoryInfoModel> Repositories { get; set; }
        public string RootPackageDir { get; set; }

        public ConfigModel()
        {
            Repositories = new List<RepositoryInfoModel>();
            RootPackageDir = "lib";
        }
    }
}
