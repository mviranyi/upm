﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPM.Model
{
    class PackageModel
    {
        public string Name { get; set; }
        public string Version { get; set; }
        public string Author { get; set; }
        public string[] Authors { get; set; }
        public string Url { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string RootPackageFolder { get; set; }

        public PackageModel()
        {
            Dependencies = new Dictionary<string, string>();
            RootPackageFolder = null;
            Name = "Unity Project";
            Version = "0.0.0";
        }
    }
}
