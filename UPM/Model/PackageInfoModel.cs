﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPM.Model
{
    class PackageInfoModel
    {
        public string name { get; set; }
        public string url { get; set; }
        public string version { get; set; }
    }
}
