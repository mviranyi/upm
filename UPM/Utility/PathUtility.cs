﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPM.Utility
{
    static class PathUtility
    {
        public static DirectoryInfo GetAssetsDirectory()
        {
            var currentPath = new DirectoryInfo(Directory.GetCurrentDirectory());
            var assetsDirectories = currentPath.GetDirectories("Assets", SearchOption.TopDirectoryOnly);

            while (assetsDirectories.Length == 0 && currentPath != null)
            {
                // not in current dir, look up
                currentPath = currentPath.Parent;
                if (currentPath!=null)
                    assetsDirectories = currentPath.GetDirectories("Assets", SearchOption.TopDirectoryOnly);
            }

            if (assetsDirectories.Length > 0)
                return assetsDirectories[0];
            else
                return null;
        }

        public static string GetConfigFilePath()
        {
            var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), ".UPM", "config.json");
            return path;
        }
    }
}
