﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPM.Model;

namespace UPM.Utility
{
    static class ConfigUtility
    {
        public static ConfigModel LoadConfig()
        {
            // check if config file exists
            var path = PathUtility.GetConfigFilePath();
            if (!File.Exists(path))
            {
                // does not exist. create one
                CreateConfigFile(path);
            }
            

            // load config
            var json = File.ReadAllText(path);
            var config = JsonConvert.DeserializeObject<ConfigModel>(json);
            return config;
        }

        private static void CreateConfigFile(string path)
        {
            ConfigModel model = new ConfigModel();
            RepositoryInfoModel defaultRepo = new RepositoryInfoModel() { Name = "public", Priority = 100, Url = "http://upm.wordtango.net/package/" };
            model.Repositories.Add(defaultRepo);
            var json = JsonConvert.SerializeObject(model);
            Directory.CreateDirectory(Path.GetDirectoryName(path));
            File.WriteAllText(path, json, Encoding.UTF8);
        }

        public static void SaveConfig(ConfigModel config)
        {
            var path = PathUtility.GetConfigFilePath();
            var json = JsonConvert.SerializeObject(config);
            File.WriteAllText(path, json, Encoding.UTF8);
        }
    }
}
