﻿using ManyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPM.Commands
{
    class UpdateCommand : ConsoleCommand
    {
        public UpdateCommand()
            : base()
        {
            this.IsCommand("update", "Alias for \"UPM install --update\"");
            this.SkipsCommandSummaryBeforeRunning();
        }

        public override int Run(string[] remainingArguments)
        {
            return Program.Main(new string[] {"install", "--update"});
        }
    }
}
