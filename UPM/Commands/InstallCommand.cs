﻿using ManyConsole;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UPM.Model;
using UPM.Package;
using UPM.Utility;

namespace UPM.Commands
{
    class InstallCommand : ConsoleCommand
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private bool update = false;

        public InstallCommand()
            : base()
        {
            this.IsCommand("install", "Installs or updates the packages defined in the packages.json file");
            this.AllowsAnyAdditionalArguments("[NAME] ...");
            this.HasOption("u|update", "Updates installed packages", v => this.update = true);
        }

        public override int Run(string[] remainingArguments)
        {
            log.Info("Installing packages");
            var package = Package.Package.LoadModelFromAssets();
            if (package == null)
            {
                log.Error("package.json does not exist in assets or asset folder could not be found");
                return -1;
            }

            // create repo list
            var config = ConfigUtility.LoadConfig();
            List<Repository> repositories = new List<Repository>();
            foreach (var repo in config.Repositories)
            {
                repositories.Add(new Repository(repo.Name, repo.Url,  repo.Priority));
            }
            repositories = repositories.OrderByDescending(o => o.Priority).ToList();

            var webClient = new WebClient();
            // get all dependencies

            if (remainingArguments.Length > 0)
            {
                var dependencies = new Dictionary<string, string>();
                foreach (var arg in remainingArguments)
                {
                    if (!package.Dependencies.ContainsKey(arg))
                    {
                        dependencies.Add(arg, "*");
                        package.Dependencies.Add(arg, "*");
                    }
                    else
                    {
                        dependencies.Add(arg, package.Dependencies[arg]);
                    }
                }
                
                // write out new package.json
                Package.Package.SaveModelToAssets(package);

                InstallPackages(dependencies, package.RootPackageFolder, repositories, webClient);

            }
            else
            {
                InstallPackages(package.Dependencies, package.RootPackageFolder, repositories, webClient);
            }

            return 0;
        }

        private void InstallPackages(Dictionary<string, string> packages, string rootPackageFolder, List<Repository> repositories, WebClient webClient)
        {
            for (int i = 0; i < packages.Count; i++)
            {
                var dependency = packages.ElementAt(i);
                var localFolderPath = Path.Combine(PathUtility.GetAssetsDirectory().FullName, rootPackageFolder, dependency.Key);

                var updating = false;
                if (Directory.Exists(localFolderPath)) // package is already installed
                {
                    if (this.update)
                    {
                        updating = true;
                        // get local version, change dependency to be bigger than that version. ignore if can't be found
                        var currentVersion = Package.Package.LoadModel(localFolderPath).Version;
                        dependency = new KeyValuePair<string, string>(dependency.Key, ">" + currentVersion);
                    }
                    else
                        continue;
                }

                log.Info("Installing package " + dependency.Key);
                PackageInfoModel data = null;

                // query the repository servers for data
                foreach (var repo in repositories)
                {
                    data = repo.CheckIfPresent(dependency);

                    if (data != null) // early exit if we found one
                    {
                        log.Info("Found package on repository " + repo.Name + " with version " + data.version);
                        break;
                    }
                }

                // download package if present
                if (data != null)
                {
                    // download
                    var filePath = Path.GetTempFileName();
                    try
                    {
                        log.Info("Downloading package");
                        webClient.DownloadFile(data.url, filePath);
                        if (updating)
                        {
                            log.Info("Deleting old package");
                            Directory.Delete(localFolderPath, true); // delete old contents
                        }

                        log.Info("Extracting package");
                        Directory.CreateDirectory(localFolderPath);
                        ZipFile.ExtractToDirectory(filePath, localFolderPath);

                        // check the just extracted json to see if there are more dependencies to be resolved
                        var downloadedPackageInfo = JsonConvert.DeserializeObject<PackageModel>(File.ReadAllText(Path.Combine(localFolderPath, "package.json")));
                        foreach (var d in downloadedPackageInfo.Dependencies)
                        {
                            // TODO add check to see if dependency is already there and if it is within version range
                            packages[d.Key] = d.Value;
                        }
                    }
                    catch (Exception e)
                    {
                        log.Error("Error while downloading " + data.name + "-" + data.version + " from " + data.url + ": " + e.Message);
                        if (Directory.Exists(localFolderPath))
                            Directory.Delete(localFolderPath, true);
                    }
                }
                else
                {
                    if (!updating)
                        log.Error("Package \"" + dependency.Key + "\" with version " + dependency.Value + " can not be found on any repository");
                    else
                        log.Info("No updated version found for package " + dependency.Key);
                }

            }
        }
    }
}
