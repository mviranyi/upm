﻿using ManyConsole;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPM.Package;
using UPM.Utility;

namespace UPM.Commands
{
    class RepoCommand : ConsoleCommand
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public RepoCommand()
            : base()
        {
            this.IsCommand("repo", "Repository functions");
            this.AllowsAnyAdditionalArguments();

        }

        public override int Run(string[] remainingArguments)
        {
            if (remainingArguments.Length > 0)
            {
                string action = remainingArguments[0];

                if (action.ToLower() == "add")
                {
                    return AddRepo(remainingArguments);
                }
                else if (action.ToLower() == "list")
                {
                    if (remainingArguments.Length != 1)
                    {
                        log.Error("Wrong amount of arguments. Usage: upm repo list");
                        return -1;
                    }

                    // write out list
                    var config = ConfigUtility.LoadConfig();
                    foreach (var repo in config.Repositories.OrderByDescending(o => o.Priority))
                    {
                        log.Info(repo.Name + "\t" + repo.Priority + "\t" + repo.Url);
                    }
                    return 0;
                }
                else if (action.ToLower() == "remove")
                {
                    if (remainingArguments.Length != 2)
                    {
                        log.Error("Wrong amount of arguments. Usage: upm repo NAME");
                        return -1;
                    }

                    var config = ConfigUtility.LoadConfig();
                    var repo = config.Repositories.FirstOrDefault(o => o.Name == remainingArguments[1]);
                    log.Info("Removing repo " + repo.Name + "\t" + repo.Priority + "\t" + repo.Url);
                    config.Repositories.RemoveAll(o => o.Name == remainingArguments[1]);
                    ConfigUtility.SaveConfig(config);
                    return 0;
                }
            }

            log.Error("Usage: upm repo add|list|remove");
            return -1;
        }

        private static int AddRepo(string[] remainingArguments)
        {
            // add repo
            if (remainingArguments.Length != 4)
            {
                log.Error("Wrong amount of arguments. Usage: upm repo add NAME PRIORITY URL");
                return -1;
            }

            var repoName = remainingArguments[1];
            int priority;
            if (!int.TryParse(remainingArguments[2], out priority))
            {
                log.Error("Priority needs to be an integer number");
                return -1;
            }

            var url = remainingArguments[3];

            // TODO do an url check

            var config = ConfigUtility.LoadConfig();
            var repo = config.Repositories.FirstOrDefault(o => o.Name.ToLower() == repoName);
            if (repo == null)
            {
                repo = new Model.RepositoryInfoModel();
                repo.Name = repoName;
                config.Repositories.Add(repo);
            }

            repo.Priority = priority;
            repo.Url = url;

            ConfigUtility.SaveConfig(config);
            log.Info("Repository " + repoName + " added to repository list");

            return 0;
        }
    }
}
