﻿using ManyConsole;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UPM.Model;
using UPM.Utility;

namespace UPM.Commands
{
    class RegisterCommand : ConsoleCommand
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private string packageLocation = null;

        public RegisterCommand()
            : base()
        {
            this.IsCommand("register", "Register a new package or version with the repository");
            this.HasAdditionalArguments(2, "repositoryName downloadUrl");
            this.HasOption("l|local=", "Use local package file for package info instead of remote package file", v => this.packageLocation = v);
        }

        public override int Run(string[] remainingArguments)
        {
            var repoName = remainingArguments[0];
            var url = remainingArguments[1];

            // check if repo name exists
            var config = ConfigUtility.LoadConfig();
            var repo = config.Repositories.FirstOrDefault(o => o.Name.ToLower() == repoName.ToLower());
            if (repo == null)
            {
                log.Error("Repository does not exist in config");
                return -1;
            }

            // check if valid url format
            if (!(url.StartsWith("file://", true, CultureInfo.CurrentCulture)
                || url.StartsWith("http://", true, CultureInfo.CurrentCulture)
                || url.StartsWith("https://", true, CultureInfo.CurrentCulture)
                || url.StartsWith("ftp://", true, CultureInfo.CurrentCulture)))
            {
                log.Error("Url must start with valid protocol. (file://, http://, https://, ftp://");
                return -1;
            }

            if (packageLocation == null)
            {
                // download package
                var packageWebClient = new WebClient();
                packageLocation = Path.GetTempFileName();
                try
                {
                    packageWebClient.DownloadFile(url, packageLocation);
                }
                catch (Exception e)
                {
                    log.Error("Error downloading package: " + e.Message);
                    return -1;
                }
            }

            string packageName;
            string version;

            // get package info
            using (ZipArchive archive = ZipFile.Open(packageLocation, ZipArchiveMode.Read))
            {
                var entry = archive.GetEntry("package.json");
                var packageInfo = JsonConvert.DeserializeObject<PackageModel>(new StreamReader(entry.Open()).ReadToEnd());
                packageName = packageInfo.Name;
                version = packageInfo.Version;
            }

            var webClient = new WebClient();
            webClient.BaseAddress = repo.Url;

            var data = new PackageInfoModel();
            data.name = packageName;
            data.version = version;
            data.url = url;

            try
            {
                webClient.Headers.Add("Content-Type", "application/json");
                var response = webClient.UploadString(packageName, "POST", JsonConvert.SerializeObject(data));
            }
            catch (WebException e)
            {
                if (e.Status == WebExceptionStatus.ProtocolError)
                {
                    var response = (HttpWebResponse)e.Response;
                    var statusCode = response.StatusCode;
                    var reader = new StreamReader(response.GetResponseStream());
                    log.Error("The server responded with code: " + statusCode + " " + reader.ReadToEnd());
                    return -1;
                }
                else
                {
                    // general connection error
                    log.Error("Error registering package: " + e.Message);
                    return -1;
                }
            }

            log.Info("Package " + packageName + " successfully registered with repository " + repoName);

            return 0;
        }
    }
}
