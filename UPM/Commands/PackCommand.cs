﻿using ManyConsole;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPM.Model;

namespace UPM.Commands
{
    class PackCommand : ConsoleCommand
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public PackCommand()
            : base()
        {
            this.IsCommand("pack", "Creates a package ready for distribution");
            this.HasAdditionalArguments(1, " destinationFolder");
        }

        public override int Run(string[] remainingArguments)
        {
            var package = Package.Package.LoadModelFromCurrentDir();
            if (package == null)
            {
                log.Error("package.json does not exist in current folder");
                return -1;
            }

            if (!Directory.Exists(remainingArguments[0]))
            {
                log.Error("Output folder does not exist");
                return -1;
            }

            // check if the needed data is there(name and version)
            if (package.Name == null || package.Version == null)
            {
                log.Error("package.json does not contain name and version");
                return -1;
            }

            var destPath = Path.Combine(remainingArguments[0], package.Name+"-"+package.Version+".zip");

            // build zip file with all the files in current dir and sub dirs
            log.Info("Writing package to " + destPath);
            ZipFile.CreateFromDirectory(Directory.GetCurrentDirectory(), destPath);

            return 0;
        }
    }
}
