﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UPM.Model;
using UPM.Utility;

namespace UPM.Package
{
    class Package
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static PackageModel LoadModel(string path)
        {
            if (path == null)
                return null;

            var packageJsonPath = Path.Combine(path, "package.json");
            if (!File.Exists(packageJsonPath))
            {
                return null;
            }

            // load package.json in current directory
            var json = File.ReadAllText(packageJsonPath);
            var package = JsonConvert.DeserializeObject<PackageModel>(json);

            return package;
        }

        public static PackageModel LoadModelFromCurrentDir()
        {
            return LoadModel(Directory.GetCurrentDirectory());
        }

        public static PackageModel LoadModelFromAssets()
        {
            return LoadModel(PathUtility.GetAssetsDirectory().FullName);
        }

        public static void SaveModelToAssets(PackageModel model)
        {
            var path = PathUtility.GetAssetsDirectory().FullName;
            if (path == null)
            {
                log.Error("Can't find assets folder");
                return;
            }

            var packageJsonPath = Path.Combine(path, "package.json");
            if (!File.Exists(packageJsonPath))
            {
                log.Error("package.json does not exist in assets folder");
                return;
            }

            log.Info("Saving package.json");
            // load package.json in current directory
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.NullValueHandling = NullValueHandling.Ignore;
            var json = JsonConvert.SerializeObject(model, Formatting.Indented, settings);
            File.WriteAllText(packageJsonPath, json);
        }
    }
}
