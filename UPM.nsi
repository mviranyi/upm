!include "EnvVarUpdate.nsh"

Name "UPM installer"
OutFile "UPM-0.1.0-alpha.exe"
InstallDir $PROGRAMFILES\UPM
RequestExecutionLevel admin

Page directory
Page instfiles

Section
	SetOutPath $INSTDIR
	
	File /x *.pdb UPM\bin\release\*.*
	
	${EnvVarUpdate} $0 "PATH" "P" "HKLM" $INSTDIR
	
	WriteUninstaller $INSTDIR\uninstall.exe
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\UPM" "DisplayName" "UPM package manager"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\UPM" "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\UPM" "QuietUninstallString" "$\"$INSTDIR\uninstall.exe$\" /S"
SectionEnd

Section "Uninstall"
  Delete $INSTDIR\uninstall.exe ; delete self (see explanation below why this works)
  Delete $INSTDIR\*.*
  RMDir $INSTDIR
  ${un.EnvVarUpdate} $0 "PATH" "R" "HKLM" $INSTDIR
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\UPM"
SectionEnd